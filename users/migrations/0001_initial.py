# Generated by Django 4.1.7 on 2023-04-16 06:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CustomedUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('password', models.CharField(max_length=128)),
                ('username', models.CharField(max_length=30, unique=True)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_superuser', models.BooleanField(default=False)),
                ('AIPic', models.CharField(default='static/img/chatgpt.jpg', max_length=128)),
                ('credit', models.FloatField(default=0.0)),
                ('modelName', models.CharField(default='ChatGPT', max_length=128)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
