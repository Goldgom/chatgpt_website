"""ai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.views.generic.base import RedirectView

from . import views
from . import captcha
urlpatterns = [
    path("", views.index,name = "home"),
    path("chatgpt", views.searchPost),
    path("clear", views.delMsg),
    path("getTemps", views.getTemps),
    path("login", views.loginF),
    path("register", views.register),
    path("logout", views.logoutF),
    path("addCredit", views.addCredit),
    path("getCredit", views.getCredit),
    path("getGPTSystems", views.getGPTSystems),
    path("changeSystem", views.changeSystem),
    path('captcha/', include('captcha.urls')),       # 图片验证码 路由
    path('refresh_captcha/', captcha.refresh_captcha),    # 刷新验证码，ajax
    path('test', views.test),    # 刷新验证码，ajax
    path("favicon.ico", RedirectView.as_view(url=r'static/img/favicon.ico')),
   # path("admin", admin.site.urls),
]
