//import { marked } from 'marked';


var csrfToken = $('meta[name="csrf-token"]').attr('content');
var textarea = document.querySelector('prompt');
var AIPic = getCookie("AIPic")
console.log(AIPic)
function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) 
  {
    var c = ca[i].trim();
    if (c.indexOf(name)==0) return c.substring(name.length,c.length);
  }
  return "";
}
function submitMsg() {
    var msg = document.getElementById("prompt").value;
    document.getElementById("submit").disabled = "ture";
    document.getElementById("submit").innerHTML = "<div class=\"spinner-border text-primary\"></div>"
    addMsg('user', msg);
    $.ajax({
        url: "/chatgpt",
        type: "POST",
        timeout: 300000,
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: $("#msgForm").serialize(),
        dataType: "JSON",
        success: function (data) {
            addMsg('assistant', data["text"])
            document.getElementById("tokens").innerHTML = data["tokens"]
            document.getElementById("credits").innerHTML = data["credits"]
            document.getElementById("submit").removeAttribute('disabled')
            document.getElementById("submit").innerHTML = "Send"
            document.getElementById("prompt").value= "";

        },
        error : function(XMLHttpRequest, textStatus, errorThrown){
            window.location.reload();
        }
        
    })

}
function loadTemps() {
    $.ajax({
        url: "/getTemps",
        type: "POST",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: { "token": 123456 },
        dataType: "JSON",
        success: function (data) {
            data["text"].forEach(i => { addMsg(i['role'], i['content']); })
        }
    })
}
function addMsg(role, msg) {
    if (role == "user") {
        var insertElement = document.createElement("a");
        insertElement.innerHTML = "<div style = \"background-color: rgb(51, 51, 51);text-align:left;margin-top : 2%;margin-bottom : 2%;margin-left:4%;\"><img class=\"img-fluid rounded\" src=\"static/img/user.jpg\" alt=\"chatgpt\"><a  style = \"margin-top : 1%;margin-left:5%;\">" +   marked.parse(msg).replace("<p>",'').replace("</p>",'') + "</a></div>"
        document.getElementById("text").appendChild(insertElement);
    } else if (role == "assistant") {
        var insertElement = document.createElement("a");
        insertElement.innerHTML = "<div style = \"background-color: rgb(60, 60, 60);text-align:left;margin-top : 2%;margin-bottom : 2%;margin-left:4%;\"><img class=\"img-fluid rounded\" src="+AIPic+" alt=\"chatgpt\"><a style = \"margin-top : 1%;margin-left:5%;\">" +   marked.parse(msg).replace("<p>",'').replace("</p>",'') + "</a></div>"
        document.getElementById("text").appendChild(insertElement);
    }
hljs.initHighlightingOnLoad()
}

function clear() {
    console.log("update")
    $.ajax({
        url: "/clear",
        type: "POST",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: { "token": 123456 },
        dataType: "JSON",
        success: function (data) {
            window.location.reload();
        }
    })
}