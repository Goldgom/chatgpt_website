var csrfToken = $('meta[name="csrf-token"]').attr('content');

function changeCaptcha() {
    $.ajax({
        url: "/refresh_captcha/",
        type: "POST",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: { "token": 123456 },
        dataType: "JSON",
        success: function (data) {
            document.getElementById("id_captcha_0").value = data["hashkey"]
            document.getElementById("id_captcha").src = data["image_url"]
        }
    })
}