var csrfToken = $('meta[name="csrf-token"]').attr('content');
var changeTo = ""
function loadSystems() {
    $.ajax({
        url: "/getGPTSystems",
        type: "POST",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: { "token": 123456 },
        dataType: "JSON",
        success: function (data) {
            data["systems"].forEach(i => { addSystem(i['name'], i['file']); })
        }
    })
}
function addSystem(name, file) {
    var insertElement = document.createElement("a");
    changeTo = name
    insertElement.innerHTML = "<button class= \"col\" style = \"width : 310px\" onclick =\"changeSystem(\'" + name + "\')\"><div class=\"card\"><div class=\"card-body\"><h4 class=\"card-title\">" + name + "</h4></div></div></button>"
    document.getElementById("systems").appendChild(insertElement);


}
function changeSystem(name) {
    $.ajax({
        url: "/changeSystem",
        type: "POST",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: { "token": 123456, "name": name },
        dataType: "JSON",
        success: function (data) {
            if (data["state"] == "ok") {
                //   document.getElementById("state").innerHTML = "切换成功";
                window.location.href = "/"
            }
        }
    })
}