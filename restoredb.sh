rm saves/* -rf
rm db.sqlite3
mkdir saves/prompts
mkdir saves/systems
mkdir saves/vars
python3 manage.py makemigrations
python3 manage.py makemigrations users
python3 manage.py migrate
pause