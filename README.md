# chatGPT _website

#### 介绍
一个开源的ChatGPT相关网站服务端。
体验地址 [chat.goldgom.top](https://chat.goldgom.top)
#### 软件架构
后端：Django
前端：jQuery bootstrap


#### 安装教程

1.  pip下载依赖包
2.  重置数据库
3.  添加自己的API_KEY在config.json中

#### 使用说明

1.  国内无法访问openai api 请在海外服务器部署


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


